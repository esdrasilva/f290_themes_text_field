import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final Color mAccentColor = Color(0xFF6c63fe);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Color.fromRGBO(41, 41, 56, 1),
        scaffoldBackgroundColor: Color.fromRGBO(41, 41, 56, 1),
        accentColor: mAccentColor,
        // textTheme: ThemeData.dark().copyWith().textTheme,
        textTheme: ThemeData.dark().copyWith().textTheme.copyWith(
              bodyText1: TextStyle(color: Colors.grey.shade400),
              bodyText2: TextStyle(color: Colors.grey.shade400),
            ),
        buttonTheme: ButtonThemeData(
          buttonColor: mAccentColor,
          textTheme: ButtonTextTheme.primary,
        ),
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(children: [
                  Text(
                    'VAIPASSAR ',
                    style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontWeight: FontWeight.w500),
                  ),
                  Icon(
                    Icons.favorite,
                    color: Theme.of(context).accentColor,
                    size: 18,
                  )
                ]),
                Image.asset('images/logo.png'),
                SizedBox(
                  height: 16,
                ),
                RichText(
                  text: TextSpan(
                    text: 'Queremos te',
                    style: TextStyle(fontSize: 20),
                    children: [
                      TextSpan(
                        text: ' ajudar ',
                        style: TextStyle(color: Color(0xFF6c63fe)),
                        children: [
                          TextSpan(
                            text: ' a passar por tudo isso!',
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                CustomTextField(texto: 'Nome completo'),
                CustomTextField(
                  texto: 'E-mail',
                  tipoTeclado: TextInputType.emailAddress,
                ),
                CustomTextField(
                  texto: 'Senha',
                  isPassword: true,
                ),
                CustomTextField(
                  texto: 'Confirme a senha',
                  isPassword: true,
                ),
                SizedBox(
                  height: 32,
                ),
                Row(
                  children: [
                    Checkbox(
                      onChanged: (bool value) {},
                      value: false,
                    ),
                    Expanded(child: Text('Lorem ipsum dolor sit ammet...')),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: MaterialButton(
                        child: Text('VOLTAR'),
                        textColor: Theme.of(context)
                            .copyWith()
                            .textTheme
                            .bodyText1
                            .color,
                        onPressed: () {},
                        shape: Border.all(
                            width: 1, color: Theme.of(context).accentColor),
                      ),
                    ),
                    SizedBox(
                      width: 32,
                    ),
                    Expanded(
                      child: RaisedButton(
                        onPressed: () {},
                        child: Text('PRÓXIMO'),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 24,
                ),
                Row(
                  children: [
                    Text('Já tem uma conta? '),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: FlatButton(
                        onPressed: () {},
                        child: Text(
                          'Entrar',
                          style: TextStyle(color: Colors.blueAccent),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CustomTextField extends StatelessWidget {
  final String texto;
  final TextInputType tipoTeclado;
  final bool isPassword;

  const CustomTextField({this.texto, this.tipoTeclado, this.isPassword});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: TextField(
        obscureText: isPassword ?? false,
        keyboardType: tipoTeclado ?? TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: texto,
          hintStyle: TextStyle(
            color: Theme.of(context).textTheme.bodyText1.color,
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Theme.of(context).textTheme.bodyText1.color,
              width: 1,
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Theme.of(context).textTheme.bodyText1.color,
              width: 1,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Theme.of(context).textTheme.bodyText1.color,
              width: 1,
            ),
          ),
        ),
      ),
    );
  }
}
